var express = require('express')

var bodyParser = require('body-parser')

var app = express()

app.use(bodyParser.json());


app.use (function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
})

app.post('/register', function (req, res) {
  res.send('Hello World!')
  console.log(req.body)
})

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})
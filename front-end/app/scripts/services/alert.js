'use strict';

/**
 * @ngdoc service
 * @name tokenApp.alert
 * @description
 * # alert
 * Service in the tokenApp.
 Hataları animasyonlu şekilde çıkartan servis
 */
angular.module('tokenApp')
  .service('alert', function alert ($rootScope ,$timeout) {
var alertTimeout;
return function(type,title,message,timeout){
    
    
    
    $rootScope.alert={
        
        hasBeenShown:true,
        show:true,
        type:type,
        message:message,
        title:title
        
    };
    
    $timeout.cancel(alertTimeout);
    alertTimeout=$timeout(function(){
        $rootScope.alert.show=false;
        
    },timeout||2000);
}
});

'use strict';

/**
 * @ngdoc overview
 * @name tokenApp
 * @description
 * # tokenApp
 *
 * Main module of the application.
 */
angular
  .module('tokenApp', ['ui.router']);



